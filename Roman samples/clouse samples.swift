//: Playground - noun: a place where people can play



import UIKit

/*

func display(_ name: String) -> Void {

    print("Hello \(name)")

}



display("Juan")



func addNumber(_ n: Int, to m: Int) -> Int {

    //print("The sum is \(n+m)")

    return n + m

}



let sum = addNumber(10, to: 20)

print("The sum is \(sum)")



func addSub(_ n: Int,_ m: Int) -> (Int, Int) {

    return (n+m, n-m)

}



let (a,s) = addSub(5,3)

print("The sum is \(a)")

print("The difference is \(s)")

*/



/*

var nums = [4,2,6,1,9,7]



func highLow(_ nums: [Int]) -> (Int, Int)? {

    if nums.count < 1 { return nil }



    var high = nums[0]

    var low = nums[0]

    var loop = 0

    for n in nums[1..<nums.count] {

        loop += 1

        print(loop)

        if high < n {

            high = n

        }

        if low > n {

            low = n

        }

    }

    return (high, low)

}

//nums = []

//print(highLow(nums))



if let hl = highLow(nums) {

    print(hl)

}

*/



/*

func areaRect(_ l:Int = 1, _ w: Int = 1) -> Int {

    return l * w

}



areaRect(10,2)

*/

/*

func add(_ nums: Int...) -> Int {

    var sum = 0

    for n in nums {

        sum += n

    }

    return sum

}

add(10,20,2,45,67)

*/

/*

func swap(_ a: inout Int, _ b: inout Int) -> Void {

    let temp = a

    a = b

    b = temp

}



var x = 10

var y = 20

print("\(x) \(y)")

swap(&x, &y)

print("\(x) \(y)")

*/



/*

func add(_ a: Int, _ b: Int) -> Int {

    return a + b

}

func sub(_ a: Int, _ b: Int) -> Int {

    return a - Int(b)

}

func myFunc(_ yourFunc: (Int, Int) -> Int, _ a: Int, _ b: Int) -> Int {

    return yourFunc(a, b)

}



myFunc(sub, 10, 5)

*/







//let someFunc:(Int, Int) -> Int = sub

//someFunc(1,2)





/*

func chooseStepFunction(backward: Bool) -> (Int) -> Int {

    func stepForward(_ input: Int) -> Int {

        return input + 1

    }

    func stepBackward(_ input: Int) -> Int {

        return input - 1

    }

    return backward ? stepBackward : stepForward

}*/





//var currentValue = -3

//let moveNearerToZero = chooseStepFunction(backward: currentValue > 0)

//print(moveNearerToZero(currentValue))

/*

var currentValue = 10

let moveNearerToZero = chooseStepFunction(backward: currentValue > 0)

while currentValue != 0 {

    print("\(currentValue)... ")

    currentValue = moveNearerToZero(currentValue)

}*/





var nums:Set<Int> = [5,3,6,2,9,1,4]

print(nums)





//: Playground - noun: a place where people can play



import UIKit



var nums:Set<Int> = [5,3,6,2,9,1,4]

/*

func otherFunc(a: Int, b: Int) -> Bool {

    return a < b

}



let myFunc = {(a: Int, b: Int) -> Bool in

    return a > b

}

*/



/*

//original form

var sort = nums.sorted(by: {(a: Int, b: Int) -> Bool in

    if a > 5 {

        return a > b

    } else {

        return false

    }

})

print(sort)

//inferring parameter and return value types from context

sort = nums.sorted(by: {(a, b) in

    return a > b

})

print(sort)



//Implicit returns from single-expression closures

sort = nums.sorted(by: {(a, b) in a > b })

print(sort)



//Shorthand argument names

sort = nums.sorted(by: {$0 > $1})

print(sort)



//Operator methods

sort = nums.sorted(by:  > )

print(sort)



//Trailing closure syntax

sort = nums.sorted(){$0 > $1}

print(sort)

*/



var arr = [4,3,5,2,1,7,0]

func sort(_ nums: [Int], _ myFunc: (Int, Int) -> Bool) -> [Int] {

    var myArr = nums

    for n in 0..<(myArr.count-1) {

        //print(n)

        for m in (n+1)..<myArr.count {

            if myFunc(myArr[m], myArr[n]) {

                let temp = myArr[n]

                myArr[n] = myArr[m]

                myArr[m] = temp

            }

        }

    }

    return myArr

}

//general form

var s = sort(arr, {(a: Int, b:Int) -> Bool in

    return a > b

})

print(s)

//inferring types

s = sort(arr, {(a, b) in return a > b })

print(s)

//implicit return type

s = sort(arr, {(a, b) in a > b })

print(s)

//shorthand arguments

s = sort(arr, {$0 > $1})

print(s)

//trailing closure

s = sort(arr){$0 < $1}

print(s)
