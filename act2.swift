//Question 1 distance calculation answer
import Glibc
var x = (204.21,200.5)
var y = (300.35,452.65)
var distance=sqrt(pow((x.0)-(y.0),2)+pow((x.1)-(y.1),2))
print("The distance is \(distance)")

//-------------------------------------------------\\

//Question 2 number of characters in a word
var str="hello world"
var numberOfletters = [Character: Int]()
for i in "abcdefghijklmnopqrstuvwxyz ".characters {
  var count=0
  for ch in str.characters{
    if i==ch{
      count+=1
      numberOfletters[ch]=count
      }
    }
}
for (letter, no) in numberOfletters {
    print("Letter \(letter) occured \(no) times")
}
//---------------------------------------------------\\

// Question 3 number of words in a string
import Foundation
var str="it is what it is"
var count=0
var wordsCount = [String: Int]()
var chuncks = str.components(separatedBy: " ")

for word in chuncks {
  if wordsCount.index(forKey: word) == nil {
    wordsCount[word]=1
  }else{
    var ct = (wordsCount[word] != nil ? wordsCount[word]!:0)
    wordsCount[word]=ct+1
  }
}
for (word, c) in wordsCount {
    print("Word '\(word)' occured \(c) times")
}
//---------------------------------------------------\\

// Question 4 answer
var x: [Int] = [-2,-1,0,1, 2]
var y :[Int]=[-2,-1,0,1,2]
// var r=arc4random(-2,2)
// print("Points along the green line x \(r)")
var index = 1 + random() % 6
print(y)
