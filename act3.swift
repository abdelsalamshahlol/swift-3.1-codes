
import Foundation

//This is the first question answer
var str="Hello World !15"
func numberOf(_ str: String)-> (Int,Int,Int) {
var c_num=0; var c_char=0; var c_letter=0;
      for n in str.unicodeScalars{
      if(n.value >= 65 && n.value <= 90) || (n.value >= 97 && n.value <= 122){
        c_letter+=1
        }
        else if(n.value >= 48 && n.value <= 57){
          c_num+=1
        }
        else{
          c_char+=1
        }
      }
      return (c_letter,c_num,c_char)
}

var result: (Int,Int,Int)=numberOf(str)
print("Number of letter: \(result.0) \nNumber of Digits: \(result.1)\nNumber of characters: \(result.2)")

////////////////////////////////////////////////////////////////////////////
// Create a program that compute the mean, median, and mode of a given number collection. (number array)
//
func myMath(number: [Float])->(Float,Float,(Float,Float)) {
  var mean:Float=0; var median: Float=0; var mode: (Float,Float)=(0,0); var sum: Float=0;
  let number=number.sorted()
    if(number.count>2 && number.count%2==0){
    median = (number[number.count/2]+number[number.count/2-1])/2
    }
    else{
      median = number[number.count/2]
    }
    sum = number.reduce(0, +)
    mean=sum/Float(number.count)
    var counts=[Float: Float]()
    number.forEach { counts[$0] = (counts[$0] ?? 0) + 1 }
    if let (value, count) = counts.max(by: {$0.1 < $1.1}) {
        mode=(value,count)
    }
  return(mean,median,mode)
}

var nums: [Float]=[1,2,3,4,3,1,1]
var result=myMath(number: nums)
print("The mean is \(result.0)\nThe median is \(result.1)\nThe mode is \(result.2.0) occured \(result.2.1) times")
//
// ////////////////////////////////////////////////////////////////////////////
//Create a program that will return the union, intersection, and symmetric difference of a two given sets.
func setOps(first: Set<Int>, second: Set<Int>)->([Int],[Int],[Int]) {
    let union: Set=first.union(second)
    let intersection: Set=first.intersection(second)
    let symmetricDifference: Set=first.symmetricDifference(second)
    return(union.sorted(),intersection.sorted(),symmetricDifference.sorted())
}
let oddDigits: Set = [1, 3, 5, 7, 9]
let evenDigits: Set = [0, 2, 4, 6, 8, 9]

let result=setOps(first: oddDigits,second :evenDigits)
print("First set \(oddDigits)\nSecond set \(evenDigits)\n\nValues of operations done:\nUnion: \(result.0)\nIntersection: \(result.1)\nSymmetric difference: \(result.2)")

////////////////////////////////////////////////////////////////////////////
/*
Create a function that returns a dictionary with a key of unique character
letters and a value of their frequency from a given string.
e.g for #4. String: the apple
return value: [t:1, h:1, e:2, a: 1, p: 2, l: 1]
*/
func frqFinder(Str: String)->([Character:Int]) {
  var numberOfletters = [Character: Int]()

  for i in "abcdefghijklmnopqrstuvwxyz ".characters {
    var count=0
    for ch in str.characters{
      if i==ch{
        count+=1
        numberOfletters[ch]=count
        }
      }
  }
    return(numberOfletters)
}
var str="red apple"
var result=frqFinder(Str: str)
print(result)
////////////////////////////////////////////////////////////////////////////
