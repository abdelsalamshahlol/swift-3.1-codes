/*
Create a function that will accept an array of integers and function that checks if a given number is positive number.
The function is used only to trim all positive numbers from a given array of integers

func onlyPositive(_ arr:[Int], (Int) -> Bool)->[Int];

write the general form and apply the following closure optimization

-Inferring parameter and return value types from context
-Implicit returns from single-expression closures
-Shorthand argument names
-Trailing closure syntax
*/

func isPos(_ num: Int)->Bool {
  return(num>0 ? true : false)
}

func onlyPositive(arr:[Int], _ isPos:(Int)->Bool)->[Int] {
  var posNums: [Int]=[]
    for num in arr {
      if(isPos(num)){
        posNums.append(num)
      }
    }
    return(posNums)
}
var numbers=[2,-5,100,4,-1]
print(onlyPositive(arr: numbers, isPos))

//-Inferring parameter and return value types from context
var infer = onlyPositive(arr: numbers, {(a) in
  return(a>0 ? true : false)
})
print(infer)

//-Implicit returns from single-expression closures
var implicit = onlyPositive(arr: numbers, {(a) in
  (a>0 ? true : false)
})
print(implicit)

//-Shorthand argument names
var shorth = onlyPositive(arr: numbers, {($0 > 0 ? true : false)
})
print(shorth)

//-Trailing closure syntax
var trailing = onlyPositive(arr: numbers){$0>0 ? true : false}
print(trailing)

////////////////////////////////////////////////////////////////////////////////
/*
Create a program using closure that will return a collection of string if a given number from an Integer array is odd or even.

e.x. Input: [4,2,6,1,4,5,9]

use map function. e.g. arr.map(.....)

Result: ["even","even","even","odd","even","odd","odd"]
**/

var stringVal={(_ nums: [Int])->[String] in
  var result :[String] = []
  for num in nums {
   result.append(num%2==0 ? "Even" : "Odd")
  }
  return(result)
}

var nums=[1,2,3,4]
print(stringVal(nums))
