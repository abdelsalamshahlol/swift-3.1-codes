/***
Create a program that will have a collection of students. The student consist of id, name, course, and GPA (General Percentage Average).
The program will then display all student list in ascending order by its GPA. (You can use structure or class)
***/
//
class Student {
  var name: String
  var id: Int
  var course: String
  var gpa: Double
  init(_ name: String, _ id: Int, _ course: String, _ gpa: Double){
    self.name=name
    self.id=id
    self.course=course
    self.gpa=gpa
  }
}

var names = ["Adam","Bob","Jack"]
var id = [23234,45536,23425]
var course = ["BSIT","MIT","BSCS"]
var gpa = [25.2,55.2,88.55,]
var students: [Student]=[Student]()

for s in 0..<3{
  students.append(Student(names[s],id[s],course[s],gpa[s]))
}
students = students.sorted(){$0.gpa<$1.gpa}
for student in students {
  print("\(student.name) has: \(student.gpa) gpa")
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
Create a class with property a property called absValue.
Provide a property observer so that when a integer value assigned to the given property it will be always converted to absolute value.
**/
class MyAbs {
  var absValue :Int = 0 {
        willSet(newAbsValue) {
            print("Converting value of \(newAbsValue) to absolute value")
        }
        didSet {
            absValue = absValue * -1
        }
      }
}
var number: MyAbs = MyAbs()
number.absValue = -4
print("The value is absolute \(number.absValue)")

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
Create a class that will compute the volume of a cube given the length of side or the lateral surface area of a cube.
Provide a property observer for the lateral surface area.
Volume: side x side x side
Lateral Surface: 6 * side * side
**/
import Glibc

class Cube {
  var lateralSurfaceArea: Double=0 {
    willSet(newVal) {

    }
    didSet {
      side=sqrt(lateralSurfaceArea/6)
      print("calculated side \(side)\u{33A1}")
    }
  }
  var side: Double=0
  func vol()->Double{
    return(side*side*side)
  }

}
var cubeVol: Cube = Cube()
// cubeVol.lateralSurfaceArea=20
cubeVol.side=6
print("volume is \(cubeVol.vol())\u{33A5}")
