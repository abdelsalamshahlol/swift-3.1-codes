import Glibc

class Cube {
  var lateralSurfaceArea: Double=0 {
    willSet(newVal) {

    }
    didSet {
      side=sqrt(lateralSurfaceArea/6)
      print("calculated side \(side)\u{33A1}")
    }
  }
  var side: Double=0
  func vol()->Double{
    return(side*side*side)
  }

}
var cubeVol: Cube = Cube()
// cubeVol.lateralSurfaceArea=20
cubeVol.side=6
print("volume is \(cubeVol.vol())\u{33A5}")
